public class Day5Task6 {
    public static void main(String[] args) {
        System.out.println("Hitung sampai 10");
        for (int i = 1; i <= 10; i++) {
            if (i == 6) {
                continue;
            }
            System.out.println(i);
        }
    }
}
