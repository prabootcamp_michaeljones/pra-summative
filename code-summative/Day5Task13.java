class Day5Task13 {
    private String namaAwal;
    private String namaAkhir;
    private String email;
    private int umur;

    public Day5Task13() {
        namaAwal = "";
        namaAkhir = "";
        email = "";
        umur = 0;
    }

    public void setData(String namaAwal) {
        this.namaAwal = namaAwal;
    }

    public void setData(String namaAwal, String namaAkhir) {
        this.namaAwal = namaAwal;
        this.namaAkhir = namaAkhir;
    }

    public void setData(String namaAwal, String namaAkhir, int umur) {
        this.namaAwal = namaAwal;
        this.namaAkhir = namaAkhir;
        this.umur = umur;
    }

    public void setData(String namaAwal, String namaAkhir, int umur, String email) {
        this.namaAwal = namaAwal;
        this.namaAkhir = namaAkhir;
        this.umur = umur;
        this.email = email;
    }

    public void printAllData() {
        System.out.println("Nama Awal   : " + namaAwal);
        System.out.println("Nama Akhir  : " + namaAkhir);
        System.out.println("Umur        : " + umur);
        System.out.println("Email       : " + email);
    }

    public static void main(String[] args) {
        Day5Task13 yongling = new Day5Task13();
        Day5Task13 michael = new Day5Task13();
        Day5Task13 sipiking = new Day5Task13();
        Day5Task13 jofin = new Day5Task13();

        yongling.setData("Yong Ling");
        michael.setData("Michael", "Jones");
        sipiking.setData("Sipiking", "Well", 18);
        jofin.setData("Jo", "Fin", 22, "jofin23@gmail.com");

        yongling.printAllData();
        System.out.println();
        michael.printAllData();
        System.out.println();
        sipiking.printAllData();
        System.out.println();
        jofin.printAllData();
        System.out.println();
    }
}
