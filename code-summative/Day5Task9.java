import java.util.HashMap;

public class Day5Task9 {
    public static void main(String[] args) {
        HashMap<String, Integer> namaCowo = new HashMap<String, Integer>();
        System.out.println("Nama-Nama Cowok: ");
        namaCowo.put("Michael", 23);
        namaCowo.put("Yunus", 19);
        namaCowo.put("Jaesung", 22);
        namaCowo.put("David", 20);

        for (String i : namaCowo.keySet()) {
            System.out.println("\nNama: " + i + "\nUmur: " + namaCowo.get(i));
        }
    }
}
