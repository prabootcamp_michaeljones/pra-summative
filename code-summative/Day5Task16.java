class Motor extends Kendaraan {
    public Motor() {
        super();
    }

    public Motor(String mesin) {
        super(mesin, 10);
    }

    public Motor(String mesin, int gigi) {
        super(mesin, gigi);
    }

    @Override
    public void start() {
        System.out.println("Mesin sudah Panas");
    }

    public void gas() {
        System.out.println("Kita Gaskan");
    }

    public static void main(String[] args) {
        Kendaraan kendaraan = new Motor("TDR4000", 2); // upcasting;
        Motor honda = new Motor("Anjasmara", 4);

        Kendaraan honda2 = (Kendaraan) honda; // upcasting;

        System.out.println("Nama Mesin Engine : " + kendaraan.getEngine());
        System.out.println("Number of Wheel : " + kendaraan.getGigi());
        kendaraan.start();
        System.out.println();

        System.out.println("Nama Mesin Engine : " + honda2.getEngine());
        System.out.println("Number of Wheel : " + honda2.getGigi());
        honda2.start();
    }
}

class Kendaraan {
    private String mesin;
    private int gigi;

    public Kendaraan() {
        mesin = "none";
        gigi = 0;
    }

    public Kendaraan(String mesin, int gigi) {
        this.mesin = mesin;
        this.gigi = gigi;
    }

    public void setGigi(int jumlahGigi) {
        gigi = jumlahGigi;
    }

    public void setEngine(String mesin) {
        this.mesin = mesin;
    }

    public String getEngine() {
        return mesin;
    }

    public int getGigi() {
        return gigi;
    }

    public void start() {
        System.out.println("Mesin tidak dipanaskan");
    };
}
