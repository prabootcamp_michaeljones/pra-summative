//Buat Database
Create Database prabootcamp-summative;

//masuk ke Database
Use prabootcamp-summative;

//buat Table product
Create Table product (
    id INT NOT NULL AUTO_INCREMENT,
    articleNumber INT NOT NULL,
    nama Varchar(25) NOT NULL UNIQUE,
    Description Text,
    Price INT,
    PRIMARY KEY (ID)
);

//buat table productImages
Create Table productImages(
    id INT NOT NULL AUTO_INCREMENT,
    productId INT NOT NULL,
    imageUrl VARCHAR(100),
    PRIMARY KEY (id)
);

//membuat foreign key untuk productImages
    Alter Table productImages
    ADD CONSTRAINT fk_productImages_product 
    FOREIGN KEY (productId) REFERENCES product(id);

//Isi Data table product
insert into product values
    (001, 1001, "Plash Speed", "Game Console + Modem", 10000000),
    (002, 1002,"Nintendo Switch V2", "Game Console Family", 3000000),
    (003, 1003,"XBOX Series S", "Game Console Microsoft", 2800000),
    (004, 1004,"Xiaomi Pad 6", "Tablet Canggih Coy", 5000000);

//Isi Data table productImages
insert into productImages (productId, imageUrl) values
    (1, "F:\Materi Prabootcamp\pra-summative\PlashSpeed.jpg"),
    (2, "F:\Materi Prabootcamp\pra-summative\Nintendo Switch.jpg"),
    (3, "F:\Materi Prabootcamp\pra-summative\Xbox Series S.jpg"),
    (4, "F:\Materi Prabootcamp\pra-summative\Xiaomi Pad 6.jpg");
    
    