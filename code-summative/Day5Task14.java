class Becak extends Kendaraan {
    @Override
    public void start() {
        System.out.println("Mencari Kendaraan Lain yang ada Mesin");
    }

    public static void main(String[] args) {
        Kendaraan kendaraan = new Kendaraan();
        Becak becak = new Becak();

        becak.start();
        kendaraan.start();
    }
}

class Kendaraan {
    private boolean engine;

    public Kendaraan() {
        engine = false;
    }

    public void setEngine(boolean engine) {
        this.engine = engine;
    }

    public boolean getEngine() {
        return engine;
    }

    public void start() {
        if (engine == false) {
            System.out.println("Becak tidak ada Engine");
        } else
            System.out.println("Becak ada Engine");
    };
}
