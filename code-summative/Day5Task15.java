class Hp extends Elektronik {
    public Hp() {
        super();
    }

    public Hp(String merk) {
        super(merk, 8);
    }

    public Hp(String merk, int ram) {
        super(merk, ram);
    }

    public void start() {
        System.out.println("Cari HP Berhasil");
    }

}

class Elektronik {
    private String merk;
    private int ram;

    public Elektronik() {
        merk = "Oddo";
        ram = 4;
    }

    public Elektronik(String merk, int ram) {
        this.merk = merk;
        this.ram = ram;
    }

    public void setRam(int totalRam) {
        ram = totalRam;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getMerk() {
        return merk;
    }

    public int getRam() {
        return ram;
    }

    public void start() {
        System.out.println("Tidak ada HP Disini");
    };

    public static void main(String[] args) {
        Hp hp = new Hp("Infinix Note 10", 8);

        System.out.println("Merk HP : " + hp.getMerk());
        System.out.println("Ram HP : " + hp.getRam());
        hp.start();
    }
}
