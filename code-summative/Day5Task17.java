class Day5Task171 implements Day5Task172, Day5Task173 {

    private int umur = 23;
    private String jenisKelamin = "Laki-laki";

    @Override
    public void setUmur(int umur) {
        this.umur = umur;
    }

    @Override
    public void setjenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public int getUmur() {
        return umur;
    }

    public String getjenisKelamin() {
        return jenisKelamin;
    }

    public static void main(String[] args) {

        Day5Task171 manusia = new Day5Task171();
        System.out.println("Nama Manusia: Michael");
        System.out.println("Umur: " + manusia.getUmur());
        System.out.println("Jenis Kelamin: " + manusia.getjenisKelamin());

    }
}

// interface 1
interface Day5Task172 {
    void setUmur(int umur);
}

// interface2
interface Day5Task173 {
    void setjenisKelamin(String jenisKelamin);
}
