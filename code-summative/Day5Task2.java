import java.util.Scanner;

public class Day5Task2 {
    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        int angka;

        System.out.print("Masukan angka anda: ");
        angka = input.nextInt();

        String messenge = angka > 10 ? "Lebih besar dari 10" : "Lebih Kecil dari 10";
        System.out.println(messenge);
    }
}
